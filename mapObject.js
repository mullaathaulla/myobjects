function mapObject(obj,cb){
    let finalObject={};
    for(let x in obj){
        finalObject[x]=cb(obj[x]);
    }
    return finalObject;
}

module.exports=mapObject;