function invert(obj){
    let finalObject={};
    for(let x in obj){
        if (typeof obj[x] === 'object' || Array.isArray(obj[x]) ) {
            finalObject[JSON.stringify(obj[x])]=x;
        }
        else{
        finalObject[obj[x]]=x;

        }
    }
    return finalObject;
}

module.exports=invert;