function defaults(obj,defaultObj){
    for(let x in defaultObj){
        if(x in obj){
            continue;
        }
        else{
            obj[x]=defaultObj[x];
        }
    }
     return obj;
}

module.exports=defaults;
